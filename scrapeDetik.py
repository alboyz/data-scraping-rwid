import requests
from bs4 import BeautifulSoup

url = requests.get('https://www.detik.com/tag/berita-terpopuler')
soup = BeautifulSoup(url.text, 'html.parser')

berita_populer =soup.find(attrs={'class':'list media_rows list-berita'})

image = berita_populer.find_all('img')

#print(image)
#image_titles = [i['title'] for i in image]
#print(image_titles)

for i in image:
    print(i['src'])