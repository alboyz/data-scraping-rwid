import requests
from bs4 import BeautifulSoup
from flask import Flask, render_template


app = Flask(__name__)


@app.route('/')
def Home():
    return render_template('index.html')


@app.route('/detik')
def DetikPopuler():
    url = requests.get('https://www.detik.com/tag/berita-terpopuler')
    soup = BeautifulSoup(url.text, 'html.parser')

    berita_populer =soup.find(attrs={'class':'list media_rows list-berita'})

    image = berita_populer.find_all('img')

    return render_template('index.html', image=image)


if __name__ == '__main__':
    app.run(debug=True)
