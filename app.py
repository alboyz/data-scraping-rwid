from bs4 import BeautifulSoup
import bs4
import requests

url = 'https://jadwalsholat.pkpu.or.id/?id=80'
page = requests.get(url)
soup = bs4.BeautifulSoup(page.content, "html.parser")
data = soup.find_all('tr', 'table_light')
print(data)
data = data[0]
data_sholat ={}
i = 0
for d in data:
    if i == 1:
        data_sholat['subuh'] = d.get_text()
    elif i == 2:
        data_sholat['zhuhur'] = d.get_text()
    elif i == 3:
        data_sholat['ashar'] = d.get_text()
    elif i == 4:
        data_sholat['maghrib'] = d.get_text()
    elif i == 5:
        data_sholat['isya'] = d.get_text()
    i += 1
    print(data_sholat)
